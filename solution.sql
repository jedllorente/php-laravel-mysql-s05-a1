-- 1. return customerName of the customers who are from Philippines and

SELECT customerName FROM customers WHERE country = "Philippines";

-- 2. return the contactLastName and contactFirstName of the customers with name "La Rochelle Gifts "

SELECT contactLastName, contactFirstName FROM customers WHERE customerName = "La Rochelle Gifts";

-- 3. return product name and MSRP of the product named "The Titanic"

SELECT productName, MSRP FROM products WHERE productName = "The Titanic";

-- 4. return first and last name of the employee whose email is "jfirrelli@classicmodelcars.com"

SELECT `lastName`, `firstName` WHERE `email` = "jfirrelli@classicmodelcars.com";

-- 5. Return the names of customers who have no registered state.

SELECT `customerName`, `contactLastName`, `contactFirstName`FROM `customers` WHERE `state` IS NULL;

-- 6. Return first name, lastname, email of employee whose last name is Patterson and first name is Steve

SELECT firstName, lastName, email FROM employees WHERE firstName ="Steve" AND lastName ="Patterson";

-- 7. return customer name, country, and credit limit of customers whose countries are NOT USA and whose credit limits are greater than 3000;

SELECT contactLastName, contactFirstName, customerName, country, creditLimit FROM customers WHERE country != "USA" AND creditLimit > 3000;

-- 8. return customer names of customers whose customer Names don't have 'a' in them.

SELECT contactFirstName, contactLastName, customerName FROM customers WHERE customerName NOT LIKE "%a%";

-- 9. return the customer number of orders whose comments contain the string "DHL".

SELECT customerNumber FROM orders WHERE comments LIKE "%DHL%";

-- 10. return the product lines whose text description mentions the phrase 'state of the art'

SELECT productLine, textDescription FROM productlines WHERE textDescription LIKE "%state of the art%";

-- 11. return countries of customers without duplication

SELECT DISTINCT country FROM customers;

-- 12. return the statuses of orders without duplication.

SELECT DISTINCT status FROM orders;

-- 13. return names and countries of customer whose country is USA, FRANCE or CANADA.

SELECT customerName, country FROM customers WHERE country IN("USA", "FRANCE", "CANADA");

-- 14. return first name, lastname and office's city of employees whose offices are in Tokyo. return

SELECT employees.firstName, employees.lastName, offices.city FROM offices JOIN employees ON offices.officeCode = employees.officeCode WHERE city = "Tokyo";

-- 15. return the customer names of customers who were served by the employee named "Leslie Thompson".

SELECT customers.customerName, employees.firstName, employees.lastName FROM employees JOIN customers ON employees.employeeNumber = customers.salesRepEmployeeNumber WHERE firstName = "Leslie" AND lastName = "Thompson";

-- 16. return the product names and customer name of products ordered by "Baane Mini Imports".

SELECT products.productName, customers.customerName FROM customers 
    JOIN orders ON customers.customerNumber = orders.customerNumber 
    JOIN orderdetails ON orders.orderNumber =  orderdetails.orderNumber
    JOIN products ON orderdetails.productCode = products.productCode
    WHERE customers.customerName = "Baane Mini Imports";

-- 17. return employees first name, employees last names, customer's names, and offices of countries of transactions whose customers and offices are in the same country.
SELECT employees.firstName, employees.lastName, customers.customerName, offices.country FROM customers
    JOIN offices ON customers.country = offices.country
    JOIN employees on offices.officeCode = employees.officeCode
    WHERE offices.country = customers.country;

-- 18. last names and first names of employees being supervised by "Anthony Bow"

SELECT firstName, lastName FROM employees WHERE reportsTo = 1056;

-- 19. product name and MSRP of the prodct with the highest MSRP.

SELECT productName, MAX(MSRP) AS MSRP FROM products;

-- 20. return number of customers in UK.

SELECT COUNT(*) FROM customers WHERE country = "UK";

-- 21. return number of products per product line

SELECT productLine, COUNT(products) FROM productlines 
    JOIN products ON productlines.productLine = products.productLine;

-- 22. return number customers served by every employee.
SELECT COUNT(*), employees.firstName, employees.lastName FROM customers 
    JOIN employees ON employees.employeeNumber = customers.salesRepEmployeeNumber;

-- 23. product name and quantity in stock of products that belong to the product line "planes" with stock quantities < 1000;
SELECT productName, quantityInStock FROM products WHERE productLine = 'planes' AND quantityInStock < 1000;